// Demo buffer overflow vulnerability
#include <stdio.h>
#include <string.h>

//___________________________
void secretFunction(char *input) {
	puts("UBER K001 B)");
}
//___________________________
void evilFunction(char *input) {
	char buffer[100];
	strcpy(buffer, input);
	puts(buffer);
}
//___________________________
int main(int argc, char **argv) {
	evilFunction(argv[1]);
	return 0;
}
